package ldh.maker;

import ldh.maker.component.JavafxClientContentUiFactory;
import ldh.maker.util.UiUtil;

public class DeskMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new JavafxClientContentUiFactory());
    }

    public static void main(String[] args) throws Exception {
        startDb(args);
        launch(args);
    }
}

