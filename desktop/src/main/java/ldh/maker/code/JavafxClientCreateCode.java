package ldh.maker.code;

import javafx.concurrent.Task;
import javafx.scene.control.TreeItem;
import ldh.database.Column;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.db.EnumDb;
import ldh.maker.freemaker.*;
import ldh.maker.util.*;
import ldh.maker.vo.JavafxSetting;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TableViewData;
import ldh.maker.vo.TreeNode;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;


/**
 * Created by ldh on 2017/4/6.
 */
public class JavafxClientCreateCode extends JavafxCreateCode{

    public JavafxClientCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        super(data, treeItem, dbName, table);
    }

    public void create() {
        if (data == null) return;
        javafxPath = data.getSettingJson().getJavafxPojoPath();
        String pojoPath = createPath(javafxPath);
        if (!table.isCreate()) return;

        if (table.isMiddle()) {
            return;
        }

        Class<?> key = null;
        if (table.getPrimaryKey() != null && !table.getPrimaryKey().isComposite()) {
            Column c = table.getPrimaryKey().getColumns().iterator().next();
            key = c.getPropertyClass();
        }
        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + dbName);

        copyResource();
        buildPomXmlMaker();
        buildLog4jPropertiesMaker(javafxPath);
        buildJavafxMainMaker(tableInfo);
        buildJavafxMaker(tableInfo);
        buildUtilFile(tableInfo, "util", "client/ConfigUtil.ftl", "ConfigUtil.java");
        buildUtilFile(tableInfo, "util", "client/JavafxHttpUtil.ftl", "JavafxHttpUtil.java");
        buildUtilFile(tableInfo, "cell", "DateTableCellFactory.ftl", "DateTableCellFactory.java");
        buildUtilFile(tableInfo, "cell", "ObjectTableCellFactory.ftl", "ObjectTableCellFactory.java");

        if (table.getPrimaryKey() != null && !table.getPrimaryKey().isComposite()) {
            JavafxPojoMaker javafxPojoMaker = buildJavafxPojoMaker(table, pojoPath, key, null);

            TableViewData tableViewData = tableInfo.getTableViewDataMap().get(table.getName());
            List<TableViewData.ColumnData> columnDatas = tableViewData.getColumnDatas();

            for (Column column : table.getColumnList()) {
                boolean isHave = false;
                for (TableViewData.ColumnData columnData : columnDatas) {
                    if (columnData.getColumnName().equals(column.getName())) {
                        isHave = true;
                        column.setCreate(columnData.getShow());
                        column.setWidth(columnData.getWidth());
                    }
                }
                if (!isHave) continue;

                String keyId = treeItem.getParent().getValue().getId() + "_" + dbName + "_" + table.getName() + "_" + column.getName();
                EnumStatusMaker enumStatusMaker = EnumFactory.getInstance().get(keyId);
                if (enumStatusMaker != null) {
                    String p = createPath(enumStatusMaker.getPack());
                    enumStatusMaker.outPath(p).make();
                    javafxPojoMaker.imports(enumStatusMaker);
                    column.setJavaType(enumStatusMaker.getSimpleName());
                }
            }

            javafxPojoMaker.make();

            buildJavafxMainFormMaker(table, "client/PojoMain.ftl");
            buildJavafxMainControllerMaker(table, "client/PojoMainController.ftl");
            buildJavafxEditFormMaker(table, "client/PojoEditForm.ftl");
            buildJavafxEditControllerMaker(table, "client/PojoEditController.ftl");
            buildJavafxSearchFormMaker(table, "client/PojoSearchForm.ftl");
            buildJavafxSearchControllerMaker(table, "client/PojoSearchController.ftl");
        }
    }

    protected void copyResource() {
        super.copyResource();
        List<String> dirs = new ArrayList<>(Arrays.asList("code", root, getProjectName(), "src", "main", "resources"));
        try {
            copyFile("config.properties", dirs);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getProjectName() {
        return "desktop-client";
    }

    protected void buildJavafxMainMaker(TableInfo tableInfo) {
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String path = this.createPath(projectPackage);
        new JavafxMaker()
                .projectPackage(projectPackage)
                .tableInfo(tableInfo)
                .ftl("client/DeskClientMain.ftl")
                .fileName("DeskClientMain.java")
                .outPath(path)
                .make();
    }

    protected void buildJavafxMaker(TableInfo tableInfo) {
        String fxml = createResourcePath("fxml");
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String path = this.createPath(projectPackage + ".controller");
        new JavafxMaker()
                .projectPackage(projectPackage)
                .tableInfo(tableInfo)
                .ftl("Home.ftl")
                .fileName("Home.fxml")
                .outPath(fxml)
                .make();

        new JavafxMaker()
                .projectPackage(projectPackage)
                .tableInfo(tableInfo)
                .ftl("client/HomeController.ftl")
                .fileName("HomeController.java")
                .outPath(path)
                .make();

        new JavafxMaker()
                .projectPackage(projectPackage)
                .tableInfo(tableInfo)
                .ftl("HomeHead.ftl")
                .fileName("HomeHead.fxml")
                .outPath(fxml)
                .make();

        new JavafxMaker()
                .projectPackage(projectPackage)
                .tableInfo(tableInfo)
                .ftl("client/HomeHeadController.ftl")
                .fileName("HomeHeadController.java")
                .outPath(path)
                .make();

        new JavafxMaker()
                .projectPackage(projectPackage)
                .tableInfo(tableInfo)
                .table(table)
                .ftl("HomeLeft.ftl")
                .fileName("HomeLeft.fxml")
                .outPath(fxml)
                .make();

        new JavafxMaker()
                .projectPackage(projectPackage)
                .tableInfo(tableInfo)
                .ftl("client/HomeLeftController.ftl")
                .fileName("HomeLeftController.java")
                .outPath(path)
                .make();
    }

    protected void buildPomXmlMaker() {
        String projectRootPackage = getProjectRootPackage(javafxPath);
        String resourcePath = createPomPath();
        new PomXmlMaker()
                .projectRootPackage(projectRootPackage)
                .project(this.getProjectName())
                .outPath(resourcePath)
                .ftl("client/pom.ftl")
                .make();
    }

}
