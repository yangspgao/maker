package ldh.maker;

import ldh.maker.component.BootstrapContentUiFactory;
import ldh.maker.util.UiUtil;

public class BootstrapWebMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new BootstrapContentUiFactory());
    }

    public static void main(String[] args) throws Exception {
        startDb(args);
        launch(args);
    }
}

